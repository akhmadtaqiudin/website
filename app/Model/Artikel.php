<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Artikel extends Model
{
    protected $guarded = [];

    public function kategori(){
    	return $this->belongsTo('App\Model\Kategori','id_kategori','id');
    }

    public function user(){
    	return $this->belongsTo('App\User','id_author','id');
    }
}
