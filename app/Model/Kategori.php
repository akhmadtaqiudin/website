<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $guarded = [];

    public function getFullKategoriAttribute(){
    	return $this->id.'-'.$this->nama_kategori;
    }
}
