<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use File;
use Image;
use DataTables;
use Carbon\Carbon;
use App\User;
use App\Model\Kategori;
use App\Model\Artikel;

class ArtikelController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function json() {
        // return DataTables::of(Artikel::all())->make(true);
        $articles = Artikel::query();

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        if($start_date && $end_date){
            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $articles->whereRaw("date(artikels.created_at) >= '".$start_date."' AND date(artikels.created_at) <= '".$end_date."'");
        }
        $articles = $articles->select('*');
        return Datatables::of($articles)
            ->addColumn('action', function($articles){
                return  '<a href="'. url('/artikel/view/'.$articles->id).'" class="btn btn-default btn-xs" style="margin:2px;"><i class="fa fa-eye"></i>  View</a>'.
                    '<a href="'. url('/artikel/'.$articles->id).'" style="margin:2px;" class="btn btn-warning btn-xs "><i class="fa fa-pencil"></i> Edit</a>'.
                    '<a href="'. url('/artikel/delete/'.$articles->id).'" style="margin:2px;" class="btn btn-danger btn-xs "><i class="fa fa-trash"></i> Delete</a>';
            })
            ->editColumn('created_at', function($articles){
                return date('d/m/Y', strtotime($articles->created_at));
            })
            ->editColumn('id_kategori', function($articles){
                return $articles->kategori->nama_kategori;
            })
            ->editColumn('id_author', function($articles){
                return $articles->user->name;
            })
        ->rawColumns(['action'])->make(true);
    }

    public function auto_search(Request $request){
        $nama = $request->nama;
        $data = [];
        $data = Kategori::select('id','nama_kategori')->where('nama_kategori','like','%'.$nama.'%')->get();
        return response()->json($data);
    }

    public function index() {
        $articles = Artikel::with(['kategori','user'])->paginate(5);
        return view('articles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $user = User::all();
        $kategori = Kategori::all();
        return view('articles.create',compact(['user','kategori']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request,[
            'id_kategori' => 'required',
            'title' => 'required|max:100',
            'content' => 'required',
            'image' => 'required|image|mimes:jpg,png,jpeg'
        ]);

        try {

            $path = public_path('uploads/images');
            $dimensions = ['150','300','500'];

            if(!File::isDirectory($path)){
                File::makeDirectory($path, 0777, true, true);
            }

            $file = $request->file('image');
            $fileName = Carbon::now()->timestamp.'-'.uniqid().'.'.$file->getClientOriginalExtension();
            Image::make($file)->save($path.'/'.$fileName);

            foreach ($dimensions as $value) {
                $canvas = Image::canvas($value, $value);

                $resizeImage = Image::make($file)->resize($value, $value, function($constraint){
                    $constraint->aspectRatio();
                });

                if(!File::isDirectory($path.'/'.$value)){
                    File::makeDirectory($path.'/'.$value);
                }

                $canvas->insert($resizeImage,'center');
                $canvas->save($path.'/'.$value.'/'.$fileName);
            }

            $user = Auth::user();
            $article = new Artikel(); //::create($request->all());
            $article->title = $request['title'];
            $article->content = $request['content'];
            $article->id_kategori = $request['id_kategori'];
            $article->id_author = $user->id;
            $article->image = $fileName;
            $article->save();
            return redirect('/artikel')->with(['success' => '<strong>' .$article->title. '</strong> Berhasil disimpan']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $article = Artikel::with(['kategori','user'])->findOrFail($id);
        // $article = Artikel::findOrFail($id);
        return view('articles.show',compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $article = Artikel::with(['kategori','user'])->findOrFail($id);
        return view('articles.edit',compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request,[
            'id_kategori' => 'required',
            'title' => 'required|max:100',
            'content' => 'required'
        ]);

        try {
            $article = Artikel::findOrFail($id);

            $user = Auth::user();            
            $article->title = $request['title'];
            $article->content = $request['content'];
            $article->id_kategori = $article->id_kategori;
            $article->id_author = $user->id;
            $article->update();//($request->all());
            return redirect('/artikel')->with(['success' => '<strong>' .$article->title. '</strong> Berhasil diubah']);
        } catch (\Exception $e) {
            return redirect()->back()->with(['error' => $e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $article = Artikel::findOrFail($id);
        $article->delete();
        return redirect('/artikel')->with(['success' => '<strong>' .$article->title. '</strong> Berhasil dihapus']);
    }
}
