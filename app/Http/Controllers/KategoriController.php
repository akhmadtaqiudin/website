<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Kategori;

class KategoriController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    
    public function index(){
    	$kategoris = Kategori::orderBy('nama_kategori','ASC')->paginate(7);
    	return view('kategoris.index',compact('kategoris'));
    }

    public function search(Request $request){
        $nama = $request->nama;
        $kategoris = Kategori::where('nama_kategori','like','%'.$nama.'%')->get();
        return view('kategoris.index',compact('kategoris'));
    }

    public function store(Request $request){
    	$this->validate($request,['nama_kategori' => 'required']);

    	try {
    		$kategori = Kategori::create($request->all());
    		return redirect('/kategori')->with(['success' => '<strong>' .$kategori->nama_kategori. '</strong> Berhasil disimpan']);
        } catch (\Exception $e) {
            return redirect('/kategori')->with(['error' => $e->getMessage()]);
        }
    }

    public function edit($id){
    	$kategori = Kategori::findOrFail($id);
    	return view('kategoris.edit',compact('kategori'));
    }

    public function update(Request $request, $id){
    	$this->validate($request,['nama_kategori' => 'required']);

    	try {
    		$kategori = Kategori::findOrFail($id);
    		$kategori->update($request->all());
    		return redirect('/kategori')->with(['success' => '<strong>' .$kategori->nama_kategori. '</strong> Berhasil diubah']);
        } catch (\Exception $e) {
            return redirect('/kategori/edit')->with(['error' => $e->getMessage()]);
        }
    }

    public function delete($id){
    	$kategori = Kategori::findOrFail($id);
    	$kategori->delete();
    	return redirect('/kategori')->with(['success' => '<strong>' .$kategori->nama_kategori. '</strong> Berhasil dihapus']);
    }
}
