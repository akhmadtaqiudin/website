<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Artikel;
use App\Model\Kategori;
use PDF;

class IndexController extends Controller
{    
    public function showAll() {
    	$new_articles = Artikel::with(['kategori','user'])->orderBy('created_at','DESC')->take(3)->get();
        $articles = Artikel::with(['kategori','user'])->take(8)->get();
        $last_articles = Artikel::with(['kategori','user'])->orderBy('created_at','ASC')->paginate(6);

        $kategori = Kategori::all();
        return view('index',compact('articles','new_articles','last_articles','kategori'));
    }

    public function detail($id) {
        $article = Artikel::with(['kategori','user'])->findOrFail($id);
        $kategori = Kategori::all();
        return view('detail',compact('article','kategori'));
    }

    public function get_kategori($id){
        $articles = Artikel::with(['kategori','user'])->where('id_kategori',$id)->get();
        $kategori = Kategori::all();
        return view('kategori',compact('articles','kategori'));
    }

    public function generatePdf($id) {
        $article = Artikel::findOrFail($id);
        $pdf = PDF::loadView('articles.viewpdf',compact('article'));//->setPaper('a4','landscape');
        return $pdf->stream();
    }
}
