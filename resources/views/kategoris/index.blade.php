@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Kategori</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="row">
                            <div class="col-md-5">
                                @if(session('error'))
                                    <div class="alert alert-danger">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {{ session('error') }}
                                    </div>
                                @endif

                                <form action="{{ url('/kategori/save') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="nama_kategori">Kategori</label>
                                        <input type="text" name="nama_kategori" class="form-control {{ $errors->has('nama_kategori') ? 'is-invalid':'' }}">
                                        <p class="text-danger">{{ $errors->first('nama_kategori') }}</p>
                                    </div>
                                    <div class="form-group">
                                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Simpan</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-7">
                                @if (session('success'))
                                    <div class="alert alert-success">
                                        <button type="button" class="close" data-dismiss="alert">×</button>
                                        {!! session('success') !!}
                                    </div>
                                @endif
                                
                                <table class="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center;">ID Kategori</th>
                                            <th>Kategori</th>
                                            <th>Full Kategori</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($kategoris as $data)
                                            <tr>
                                                <td style="text-align: center;">{{ $data->id }}</td>
                                                <td>{{ $data->nama_kategori }}</td>
                                                <td>{{ $data->full_kategori }}</td>
                                                <td>
                                                    <form action="{{ url('/kategori/delete/'.$data->id) }}" method="POST">
                                                        @csrf
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <a href="{{ url('/kategori/'.$data->id) }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Edit</a>
                                                        <button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
