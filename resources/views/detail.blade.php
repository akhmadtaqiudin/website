@extends('layouts.header')

@section('content')
	<div class="container main-content">
		<div class="row">
			<div class="col-md-9 detail-content">
				<div>
					<figure class="figure">
						@if (!empty($article->image))
							<img src="{{ asset('uploads/images/500/'.$article->image) }}" class="figure-img img-fluid rounded" alt="loading">
						@else
							<img src="{{ asset('images/spring-logo.jpg') }}" class="figure-img img-fluid rounded" alt="loading">
						@endif

						<figcaption class="figure-caption"><h2>{{ $article->title }}</h2></figcaption>
					</figure>
				</div>
				
				<div class="card-footer text-muted">
			    	<p style="margin-bottom: 0px">Kategori : {{ $article->kategori->nama_kategori }}</p>
			    	<p style="margin-bottom: 0px">Tulisan ini tersedia dalam bentuk <a href="{{ url('pdf/'.$article->id) }}"><i class="fa fa-file-pdf-o"></i> Pdf</a></p>
			  	</div>

				<p>{!! $article->content !!}</p> 

			</div>
			<div class="col-md-3">
				<img src="https://thumbs.dreamstime.com/b/spring-logo-design-13350112.jpg" class="card-img-top" alt="Loading" width="100%">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				<br><br>
				<div class="custom-list">
					<h3 class="custom-text">Kategori</h3>
					@foreach($kategori as $data)
						<p class="p-text"><a href="{{ url('detail-kategori/'.$data->id) }}">{{ $data->nama_kategori }}</a></p>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection