@extends('layouts.header')

@section('content')
	<header class="masthead">
	  	<div class="container h-100">
	    	<div class="row h-100 align-items-center">
	      		<div class="col-12 text-center">
	        		<h1 class="font-weight-light">Vertically Centered Masthead Content</h1>
	        		<p class="lead">A great starter layout for a landing page</p>
	      		</div>
	    	</div>
	  	</div>
	</header>

	<di class="row">
		<div class="container">
			<div class="row marginfix">
				<div class="col-sm-8">	
					<!-- carousel -->
					<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
						    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
						    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
						</ol>
					  	<div class="carousel-inner">
						    <div class="carousel-item active">
						      	<img src="{{ asset('images/150x90.png') }}" class="d-block w-100" alt="...">
						      	<div class="carousel-caption d-none d-md-block">
						          	<h5>First slide label</h5>
						        </div>
						    </div>
						    @foreach($new_articles as $data)
							    <div class="carousel-item">
							    	@if (!empty($data->image))
										<img src="{{ asset('uploads/images/'.$data->image) }}" class="d-block w-100" alt="loading">
									@else
										<img src="{{ asset('images/150x90.png') }}" class="d-block w-100" alt="loading">
									@endif
									<div class="carousel-caption d-none d-md-block">
							          	<h5>{{ $data->title }}</h5>
							        </div>
							    </div>
						    @endforeach
					  	</div>
					  	<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
						    <span class="sr-only">Previous</span>
					  	</a>
					  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true"></span>
						    <span class="sr-only">Next</span>
					  	</a>
					</div>		
					<!-- end carousel -->
					<!-- start content -->

					<div class="marginfix"></div>
					<div class="row">
						@foreach($articles as $data)
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="card text-center">
								@if (!empty($data->image))
									<img src="{{ asset('uploads/images/'.$data->image) }}" class="card-img-top" alt="loading">
								@else
									<img src="{{ asset('images/spring-logo.jpg') }}" class="card-img-top" alt="loading">
								@endif
							  	<div class="card-body">
							    	<h5 class="card-title">{{ str_limit($data->title, $limit = 20, $end = '...') }}</h5>			    
							  	</div>
							  	<footer class="blockquote-footer">
							  		Kategori : <cite title="Source Title">{{ $data->kategori->nama_kategori }}</cite><br>
							  		Peulis : <cite title="Source Title">{{ $data->user->name }}</cite><br>
							  		<i class="fa fa-clock-o"></i> {{ date('d-m-Y', strtotime($data->created_at))}}<br>
							  	</footer>
							  	<div class="card-footer text-muted">
							    	<a href="{{ url('/detail-artikel/'.$data->id) }}" class="btn btn-info">Baca Lebih lanjut</a>
							  	</div>
							</div>
						</div>
						@endforeach
					</div>

					<div class="row">
						@foreach($last_articles as $data)
							<div class="col-md-3 col-sm-6 col-xs-12">
								<div class="card">
									@if (!empty($data->image))
										<img src="{{ asset('uploads/images/'.$data->image) }}" class="card-img-top" alt="loading">
									@else
										<img src="{{ asset('images/spring-logo.jpg') }}" class="card-img-top" alt="loading">
									@endif
								  	<div class="card-body">
								    	<h5 class="card-title"><a href="{{ url('/detail-artikel/'.$data->id) }}">{{ str_limit($data->title, $limit = 17, $end = '...') }}</a></h5>			    
								  	</div>
								  	<footer class="blockquote-footer">
								  		Kategori : <cite title="Source Title">{{ $data->kategori->nama_kategori }}</cite><br>
								  		Peulis : <cite title="Source Title">{{ $data->user->name }}</cite>
								  	</footer>
								</div>
							</div>
						@endforeach
					</div>

					<div class="pagination justify-content-end">
						{{ $last_articles->render() }}
					</div>
				</div>

				<div class="col-sm-4">
					<img src="{{ asset('images/spring-logo.jpg') }}" class="rounded img-fluid img-thumbnail" width="100%"><br><br>
					<div class="custom-list">
						<h3 class="custom-text">Kategori</h3>
						@foreach($kategori as $data)
							<p class="p-text"><a href="{{ url('detail-kategori/'.$data->id) }}">{{ $data->nama_kategori }}</a></p>
						@endforeach
					</div>
					<div class="marginfix"></div>
						
					<div id="caleandar"></div>
				</div>
			</div>	
		</div>
	</di>
@endsection