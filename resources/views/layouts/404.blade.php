<!DOCTYPE html>
<html>
    <head>
        <title>Page Not Found.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,"Noto Sans",sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
                height: 100%;}
            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }
            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }
            .content {
                text-align: center;
                display: inline-block;
            }
            .title {
                font-size: 72px;
                margin-bottom: 40px;
                text-shadow: 2px 2px #069b33;
            }
            a{text-decoration: none;}
            .btn{
                display: inline-block;
                font-weight: 400;
                text-align: center;
                vertical-align: middle;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                background-color: transparent;
                border: 1px solid transparent;
                border-top-color: transparent;
                border-right-color: transparent;
                border-bottom-color: transparent;
                border-left-color: transparent;
                padding: .375rem .75rem;
                font-size: 1rem;
                line-height: 1.5;
                border-radius: .25rem;
                transition: color .15s ease-in-out,background-color .15s ease-in-out,
                border-color .15s ease-in-out,box-shadow .15s ease-in-out;
                color: #fff;
                background-color: #069b33;
                border-color: #069b33;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <h5 class="title">Oops.. Halaman tidak ditemukan.</h5>
                <a href="{{ url('/home') }}" class="btn btn-suc">Kembali ke Halaman Awal</a>
            </div>
        </div>
    </body>
</html>
