@extends('layouts.header')

@section('content')
	<div class="container main-content">
		<div class="row">
			<div class="col-md-9 detail-content">
				<div class="row">
					@foreach($articles as $data)
						<div class="col-md-4 col-sm-6 col-xs-12">
							<div class="card text-center">
								@if (!empty($data->image))
									<img src="{{ asset('uploads/images/'.$data->image) }}" class="card-img-top" alt="loading">
								@else
									<img src="{{ asset('images/spring-logo.jpg') }}" class="card-img-top" alt="loading">
								@endif
							  	<div class="card-body">
							    	<h5 class="card-title">{{ str_limit($data->title, $limit = 20, $end = '...') }}</h5>			    
							  	</div>
							  	<footer class="blockquote-footer">
							  		Kategori : <cite title="Source Title">{{ $data->kategori->nama_kategori }}</cite><br>
							  		Peulis : <cite title="Source Title">{{ $data->user->name }}</cite>
							  	</footer>
							  	<div class="card-footer text-muted">
							    	<a href="{{ url('/detail-artikel/'.$data->id) }}" class="btn btn-info">Baca Lebih lanjut</a>
							  	</div>
							</div>
						</div>
					@endforeach
				</div>
			</div>
			<div class="col-md-3">
				<img src="https://thumbs.dreamstime.com/b/spring-logo-design-13350112.jpg" class="card-img-top" alt="Loading" width="100%">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				<br><br>
				<div class="custom-list">
					<h3 class="custom-text">Kategori</h3>
					@foreach($kategori as $data)
						<p class="p-text"><a href="{{ url('detail-kategori/'.$data->id) }}">{{ $data->nama_kategori }}</a></p>
					@endforeach
				</div>
			</div>
		</div>
	</div>
@endsection