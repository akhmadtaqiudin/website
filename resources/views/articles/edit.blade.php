@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Ubah Data Artikel</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                    	<!-- menamppilkan error apabila terdapat flash message error -->
						@if(session('error'))
							<div class="alert alert-danger">
								<button type="button" class="close" data-dismiss="alert">×</button>
								{{ session('error') }}
							</div>
						@endif

						<form action="{{ url('/artikel/update/'.$article->id) }}" method="POST">
							@csrf
							<div class="form-group">
								<label for="title">Titel Artikel</label>
								<input type="text" name="title" value="{{ $article->title }}" class="form-control {{ $errors->has('title') ? 'is-invalid':'' }}">
								<p class="text-danger">{{ $errors->first('title') }}</p>
							</div>
							<div class="form-group">
								<label for="content">Conent</label>
								<textarea id="editor1" name="content" cols="10" rows="10" class="form-control {{ $errors->has('content') ? 'is-invalid':'' }}">{{ $article->content }}</textarea>
								<p class="text-danger">{{ $errors->first('content') }}</p>
							</div>
							<div class="form-group">
								<label for="id_kategori">Kategori</label>
								<input type="text" name="id_kategori" value="{{ $article->kategori->nama_kategori }}" class="form-control" readonly="true">
							</div>
							<div class="form-group">
								<a href="{{ url('/artikel') }}" class="btn btn-info btn-sm"><i class="fa fa-mail-reply"></i> Kembali</a>
								<button class="btn btn-success btn-sm"><i class="fa fa-paper-plane"></i> Ubah</button>
							</div>
						</form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection