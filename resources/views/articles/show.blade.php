@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Detail Artikel</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
						<div class="form-group">
							<label for="title">Titel Artikel</label>
							<input type="text" name="title" value="{{ $article->title }}" class="form-control" readonly="true">
						</div>
						<div class="form-group">
							<label for="content">Conent</label>
							<textarea id="editor1" name="content" cols="10" rows="10" class="form-control" readonly="true">{{ $article->content }}</textarea>
						</div>
						<div class="form-group">
							<label for="id_kategori">Kategori</label>
							<input type="text" name="id_kategori" value="{{ $article->kategori->nama_kategori }}" class="form-control" readonly="true">
						</div>
						<div class="form-group">
							<label for="id_author">Penulis</label>
							<input type="text" name="id_author" value="{{ $article->user->name }}" class="form-control" readonly="true">
						</div>
						<div class="form-group">
							<a href="{{ url('/artikel') }}" class="btn btn-info btn-sm"><i class=""></i>Kembali</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection