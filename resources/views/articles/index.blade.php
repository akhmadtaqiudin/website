@extends('layouts.app')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Data Artikel</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <a href="{{ url('/artikel/add') }}" ><abbr title="tambah data"><i class="fa fa-plus"></i></abbr></a>
                            </li>
                            <li>
                                <a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                            </li>
                            <li>
                                <a class="close-link"><i class="fa fa-close"></i></a>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="well">
                            <div class="row">
                                <div class="col-md-4">
                                    Start Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                                <div class="col-md-4">
                                    End Date
                                    <form class="form-horizontal">
                                        <fieldset>
                                            <div class="control-group">
                                                <div class="controls">
                                                    <div class="input-prepend input-group">
                                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                        <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" />
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>                                
                                <div class="col-md-4">
                                    <button style="margin-top: 17px;" id="search" class="btn btn-info"><i class="fa fa-search"></i> Search</button>
                                </div>
                            </div>
                        </div>

                        @if (session('success'))
                            <div class="alert alert-success">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                {!! session('success') !!}
                            </div>
                        @endif

                        <table class="table table-striped table-hover table-bordered" id="artikel-table" style="width: 100% !important;">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Title</th>
                                    <th>Kategori</th>
                                    <th>Penulis</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>                                    
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@push('scripts')
<script>
    $(document).ready( function () {
        document.querySelector("#start_date").valueAsDate = new Date();
        document.querySelector("#end_date").valueAsDate = new Date();
        $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#artikel-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ url('artikel/all') }}",
                type: 'GET',
                data: function (d) {
                    d.start_date = $('#start_date').val();
                    d.end_date = $('#end_date').val();
                }
            },
            columns:[
                { data: 'created_at', name: 'created_at' },
                { data: 'title', name: 'title' },
                { data: 'id_kategori', name: 'id_kategori' },
                { data: 'id_author', name: 'id_author' },
                { data: 'action', name: 'action' }
            ],
            columnDefs:[
                { "width": "10%", "targets": 0 },
                { "width": "10%", "targets": 2 },
                { "width": "10%", "targets": 3 },
                { "width": "20%", "targets": 4 }
            ]
        });
    });

    $('#search').click(function(){
        $('#artikel-table').DataTable().draw(true);
    });
</script>
@endpush