<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Website</title>
	<style type="text/css">
		*{ font-family: DejaVu Sans !important;}
		body{
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
            color:#333;
            text-align:justify;
            font-size:16px;
            margin:0;
        }
        .container{
            margin:0 auto;
            margin-top:5px;
            padding:40px;
            height:auto;
            background-color:#fff;
        }
	</style>
</head>
<body>
	<div class="container">
		<h2>{{ $article->title }}</h2><hr>
		<p>{!! $article->content !!}</p>
	</div>
</body>
</html>