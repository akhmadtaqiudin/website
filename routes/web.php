<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@showAll');
Route::get('/detail-artikel/{id}', 'IndexController@detail');
Route::get('/detail-kategori/{id}', 'IndexController@get_kategori');
Route::get('/pdf/{id}', 'IndexController@generatePdf');

Route::get('/mlebu', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'kategori'], function(){
	Route::get('/', 'KategoriController@index');
	Route::get('/search', 'KategoriController@search');
	Route::post('/save', 'KategoriController@store');
	Route::get('/{id}', 'KategoriController@edit');
	Route::post('/update/{id}', 'KategoriController@update');
	Route::delete('/delete/{id}', 'KategoriController@delete');
});

Route::group(['prefix' => 'artikel'], function(){
	Route::get('/', 'ArtikelController@index');
	Route::get('/all', 'ArtikelController@json');
	Route::get('/add', 'ArtikelController@create');
	Route::get('/auto', 'ArtikelController@auto_search');
	Route::post('/save', 'ArtikelController@store');
	Route::get('/{id}', 'ArtikelController@edit');
	Route::post('/update/{id}', 'ArtikelController@update');
	Route::get('/view/{id}', 'ArtikelController@show');
	Route::get('/delete/{id}', 'ArtikelController@destroy');
});