## Aplikasi

Program sederhana membuat website dengan [laravel](https://laravel.com/) dan database [Postgresql](https://www.postgresql.org/)

## Cara Install
1. buka terminal / git bash
2. ``` git clone https://gitlab.com/akhmadtaqiudin/website.git ```
3. ``` cd website ```
4. ``` composer install ```
5. ``` cp .env.example .env ```
6. Setting **database** pada file ``` .env ```
7. sesuaikan 
	``` 
	DB_CONNECTION=pgsql
	DB_HOST=127.0.0.1
	DB_PORT=5432
	DB_DATABASE=nama_database
	DB_USERNAME=user_database
	DB_PASSWORD=password_database	
	```


## License

The Laravel framework is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
